package spec_builders;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.Test;
import pojo.Location;
import pojo.PostLocation;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;

public class PostPlaceTest {

    @Test(description = "Post a place | Illustrate Request and Response Builders concepts")
    public void PostPlaceTest() {

        RestAssured.baseURI = "https://rahulshettyacademy.com";

        PostLocation postLocation = new PostLocation();
        postLocation.setAccuracy(50);
        postLocation.setAddress("New Address set in test");
        postLocation.setLanguage("English");
        postLocation.setPhone_number("  983 893 3937");
        postLocation.setWebsite("http://google.com");
        postLocation.setName("New name set in test");

        List<String> myList = new ArrayList<String>();
        myList.add("first element");
        myList.add("second element");
        postLocation.setTypes(myList);

        Location locationObj = new Location();
        locationObj.setLat(-38.383494);
        locationObj.setLng(-40.383440);
        postLocation.setLocation(locationObj);

        RequestSpecification request = new RequestSpecBuilder()
                .setBaseUri("https://rahulshettyacademy.com")
                .addQueryParam("key", "qaclick123")
                .setContentType(ContentType.JSON).build();

        ResponseSpecification responseSpecBuilder = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectContentType(ContentType.JSON).build();

        RequestSpecification res =
                given()
                        .spec(request)
                        .body(postLocation);

        Response response =
                res.when()
                .post("/maps/api/place/add/json")
                .then()
                .spec(responseSpecBuilder)
                        .extract().response();

        String responseString = response.asString();

        System.out.println("Response is: " + responseString);


    }
}
