import files.Payload;
import helpers.ReusableMethods;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class PlacesTest {

    @Test(description = "Add place, update place with new address, get place to validate if the new address is present in response")
    public void PlacesTest() {

        // Add place
        RestAssured.baseURI = "https://rahulshettyacademy.com";
        // 'given' belongs to a static package that is manually imported
        String response =
                given()
                        .log().all()
                        .queryParam("key", "qaclick123")
                        .header("Content-Type", "application/json")
                        .body(Payload.AddPlace(40, "New Name"))
                        .when()
                        .post("maps/api/place/add/json")
                        .then()
                .log().all()
                        .assertThat()
                        .statusCode(200)
                        .body("scope", equalTo("APP"))
                        .header("server", "Apache/2.4.18 (Ubuntu)").extract().response().asString();

        System.out.println("Response is: " + response);
        JsonPath js = new JsonPath(response); //for parsing JSON
        String placeId = js.get("place_id");

        System.out.println("Place id is: " + placeId);

        // Update place
        given()
                .log().all()
                .queryParam("key","qaclick123")
                .header("Content-Type", "application/json")
                .body("{\n" +
                        "    \"place_id\": \""+placeId+"\",\n" +
                        "    \"address\": \"70 Summer walk, USA\",\n" +
                        "    \"key\": \"qaclick123\"\n" +
                        "}")
                .when()
                .put("/maps/api/place/get/json")
                .then()
                .log().all()
                .assertThat()
                .statusCode(200);

        String newAddress = "29, side layout, cohen 09";

        // Get place
        String getPlaceResponse =
                given()
                .log().all()
                .queryParam("key", "qaclick123")
                .queryParam("place_id", placeId)
                .when()
                .get("/maps/api/place/get/json")
                .then()
                .assertThat()
                .statusCode(200).extract().response().asString();

        JsonPath json = ReusableMethods.rawToJson(getPlaceResponse);
        String actualAddress = json.getString("address");

        System.out.println("Actual address is: " + actualAddress);

        Assert.assertEquals(actualAddress, newAddress);

    }
}
