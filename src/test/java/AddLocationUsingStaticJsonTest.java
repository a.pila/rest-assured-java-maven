import static io.restassured.RestAssured.given;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class AddLocationUsingStaticJsonTest {

    @Test(description = "Add location")
    public void addLocation() throws IOException {

        RestAssured.baseURI = "https://rahulshettyacademy.com";
        String response =
                given().
                header("Content-Type","application/json").
                body(GenerateStringFromResource("src/main/java/files/addLocation.json"))
                .when()
                .post("maps/api/place/add/json")
                .then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .extract().response().asString();

        System.out.println("Response is: " + response);
    }

    public static String GenerateStringFromResource(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }
}
