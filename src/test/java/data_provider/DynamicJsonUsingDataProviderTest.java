package data_provider;

import files.Payload;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class DynamicJsonUsingDataProviderTest {

    @Test(dataProvider = "BooksData", description = "Perform a POST using Data Provider concept")
    public void AddPlace_DataProvider_Test(int accuracy, String name) {
        RestAssured.baseURI = "https://rahulshettyacademy.com";
        // 'given' belongs to a static package that is manually imported
        String response =
                given()
                        .log().all()
                        .queryParam("key", "qaclick123")
                        .header("Content-Type", "application/json")
                        .body(Payload.AddPlace(accuracy, name))
                        .when()
                        .post("maps/api/place/add/json")
                        .then()
                        .log().all()
                        .assertThat()
                        .statusCode(200)
                        .body("scope", equalTo("APP"))
                        .header("server", "Apache/2.4.18 (Ubuntu)").extract().response().asString();

        System.out.println("Response is: " + response);
        JsonPath js = new JsonPath(response); //for parsing JSON
        String placeId = js.get("place_id");

        System.out.println("Place id is: " + placeId);
    }

        @DataProvider(name="BooksData")
                public Object[][] getData() {
                return new Object[][] { {11, "Name1"}, {12, "Name2"}, {13, "Name3"} };
        }


    }

