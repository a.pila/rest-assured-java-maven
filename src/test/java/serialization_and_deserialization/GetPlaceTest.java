package serialization_and_deserialization;

import files.Payload;
import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.path.json.JsonPath;
import pojo.GetLocation;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class GetPlaceTest {

    public static void main(String[] args) {

        RestAssured.baseURI = "https://rahulshettyacademy.com";

        String response =
                given()
                        .log().all()
                        .queryParam("key", "qaclick123")
                        .header("Content-Type", "application/json")
                        .body(Payload.AddPlace(40, "New Name using POJOs"))
                        .when()
                        .post("maps/api/place/add/json")
                        .then()
                        .log().all()
                        .assertThat()
                        .statusCode(200)
                        .body("scope", equalTo("APP"))
                        .header("server", "Apache/2.4.18 (Ubuntu)")
                        .extract().response().asString();

        JsonPath js = new JsonPath(response); //for parsing JSON
        String placeId = js.get("place_id");

        // Get place
        GetLocation gc =
                given()
                .log().all()
                .queryParam("key", "qaclick123")
                .queryParam("place_id", placeId)
                        .expect().defaultParser(Parser.JSON)
                .when()
                .get("/maps/api/place/get/json")
                .then()
                .assertThat()
                .statusCode(200)
                        .extract().response().as(GetLocation.class);

        System.out.println("Get Location: " + gc.getName());

        System.out.println("Get latitude: " + gc.getLocation().getLat());
    }
}
