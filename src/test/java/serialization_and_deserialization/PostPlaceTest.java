package serialization_and_deserialization;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import pojo.Location;
import pojo.PostLocation;
import java.util.ArrayList;
import java.util.List;
import static io.restassured.RestAssured.given;

public class PostPlaceTest {

    @Test(description = "Post place | Illustrate serialization and deserialization concepts")
    public void PostPlaceTest() {

        RestAssured.baseURI = "https://rahulshettyacademy.com";

        PostLocation postLocation = new PostLocation();
        postLocation.setAccuracy(50);
        postLocation.setAddress("New Address set in test");
        postLocation.setLanguage("English");
        postLocation.setPhone_number("  983 893 3937");
        postLocation.setWebsite("http://google.com");
        postLocation.setName("New name set in test");

        List<String> myList = new ArrayList<String>();
        myList.add("first element");
        myList.add("second element");
        postLocation.setTypes(myList);

        Location locationObj = new Location();
        locationObj.setLat(-38.383494);
        locationObj.setLng(-40.383440);
        postLocation.setLocation(locationObj);

        Response response =
                given()
                        .log().all()
                .queryParam("key", "qaclick123")
                .body(postLocation)
                .when()
                .post("/maps/api/place/add/json")
                .then()
                .assertThat()
                .statusCode(200)
                .extract().response();

        String responseString = response.asString();

        System.out.println("Response is: " + responseString);

    }
}
