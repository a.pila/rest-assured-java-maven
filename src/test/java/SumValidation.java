import files.Payload;
import io.restassured.path.json.JsonPath;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SumValidation {

    @Test(description = "Sum validation")
    public void sumOfCourses() {

        JsonPath json = new JsonPath(Payload.CoursePrice());

        int count = json.getInt("courses.size()");

        int sum = 0;

        for (int i = 0; i < count; i++) {

           int price = json.getInt("courses["+i+"].price");
           int copies = json.getInt("courses["+i+"].copies");
           int amount = price * copies;
            System.out.println("Amount: " + amount);

            sum = sum + amount;
        }
        System.out.println("Total sum: " + sum);

        int purchaseAmount = json.getInt("dashboard.purchaseAmount");
        System.out.println("Purchase amount from response: " + purchaseAmount);
        Assert.assertEquals(sum, purchaseAmount);

    }
}
