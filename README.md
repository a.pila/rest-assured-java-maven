# Automation testing framework based on Rest Assured, Java and Postman

# Test suite for 
This repository contains the concepts illustrated in this Udemy course:
https://www.udemy.com/course/rest-api-automation-testing-rest-assured/

### Tools used:
- Rest assured (for BDD testing)
- Java
- Maven
- Postman
- TestNG

### Concepts illustrated in this suite of tests:
- Assertions on Json Response Body and Headers
- Parse Json Response body using JsonPath class
- Create tests for Get, Post, Put HTTP methods
- Retrieve Json Array using JsonPath
- Iterate over elements of Json Array and access elements
- Retrieve Json Nodes on condition logic using JsonPath
- TestNG data provider for parametrization
- Session Filter
- Send attachments using MultiPart method
- Query and Path params
- Parse Complex nested Json using POJO classes
- Serialization and deserialization
- Spec builders


### 1. Functional tests

